import type { RouteRecordRaw } from 'vue-router';

export const staticRoutes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      hidden: true,
    },
  },

  {
    path: '/404',
    name: '404',
    component: () => import('@/views/error/404.vue'),
    meta: {
      hidden: true,
    },
  },

  // 对于咱们的一级路由而言生成一级菜单 二级路由来去生成二级菜单
  // 如果一级路由重只有一个二级路由 那么二级路由生成的菜单会直接替代调一级路由生成的菜单
  {
    path: '/',
    component: () => import('@/layout/index.vue'),
    redirect: '/home',
    children: [
      {
        path: 'home',
        name: 'Home',
        component: () => import('@/views/home/index.vue'),
        meta: {
          title: '首页', // 标题
          icon: 'ele-HomeFilled', //图标
        },
      },
    ],
  },
];

export const allAsyncRoutes: Array<RouteRecordRaw> = [
  // mock数据管理

  {
  path:'/mock',
  name:'Mock',
  component:()=>import('@/layout/index.vue'),
  redirect:'/mock/list',
  meta:{
       title:'mock数据管理',
       icon:'el-icon-star-off'},
       children:[
        {
          path:'NewsManagement/list',
          component:()=>import('@/views/mock/NewsManagement/index.vue'),
          meta:{
            title:'新闻管理'
          }
        },
        {
          path:'OrderManagement/list',
          component:()=>import('@/views/mock/OrderManagement/index.vue'),
          meta:{
            title:'订单管理'
          }
        },
        {
          path:'UserManagement/list',
          component:()=>import('@/views/mock/UserManagement/index.vue'),
          meta:{
            title:'用户管理'
          }
        }
  ]
   
  },
  {
    path: "/acl",
    name: "Acl",
    component: () => import("@/layout/index.vue"),
    redirect: "/acl/user/list",
    meta: {
      title: "权限管理",
      icon: "ele-Setting",
    },
    children: [
      {
        name: "User",
        path: "/acl/user/list",
        component: () => import("@/views/acl/user/index.vue"),
        meta: {
          title: "用户管理",
        },
      },
      {
        name: "Role",
        path: "/acl/role/list",
        component: () => import("@/views/acl/role/index.vue"),
        meta: {
          title: "角色管理",
        },
      },
      {
        name: "RoleAuth",
        path: "/acl/role/auth",
        component: () => import("@/views/acl/role/roleAuth.vue"),
        meta: {
          title: "角色管理",
          hidden: true,
          activeMenu: "/acl/role/list",
        },
      },
      {
        name: "Permission",
        path: "/acl/permission/list",
        component: () => import("@/views/acl/permission/index.vue"),
        meta: {
          title: "菜单管理",
        },
      },
    ],
  },

  {
    path: '/product',
    component: () => import('@/layout/index.vue'),
    name: 'Product',
    meta: {
      title: '商品管理', // 标题
      icon: 'ele-Goods', //图标
    },
    children: [
      {
        path: 'trademark/list',
        component: () => import('@/views/product/trademark/index.vue'),
        meta: {
          title: '品牌管理', // 标题
        },
        name: 'Trademark',
      },
      {
        path: 'attr/list',
        //component: () => import('@/views/product/attr/index.vue'),
        component: () => import('@/views/product/attr/index.vue'),
        meta: {
          title: '平台属性管理', // 标题
        },
        name: 'Attr',
      },
      {
        path: 'spu/list',
        component: () => import('@/views/product/spu/index.vue'),
        meta: {
          title: 'spu管理', // 标题
        },
        name: 'Spu',
      },
      {
        path: 'sku/list',
        component: () => import('@/views/product/sku/index.vue'),
        meta: {
          title: 'sku管理', // 标题
        },
        name: 'Sku',
      },

      {
        path: 'scoped/list',
        component: () => import('@/views/product/scoped/index.vue'),
        meta: {
          title: 'scoped测试',
        },
      },
    ],
  },
  {
    path: '/test',
    component: () => import('@/layout/index.vue'),
    name: 'Test',
    meta:{
      title:'测试管理',
      icon:'ele-Goods'
    },
    children:[
      {
        path:'test111/list',
        component: () => import('@/views/test/test111/index.vue'),
        meta:{
          title:'测试管理111',//标题
        },
        name:'Test1'
      },
      {
        path:'test222/list',
        component: () => import("@/views/test/test222/index.vue"),
        meta: {
          title: "测试管理222", // 标题
        },
        name:'Test2'
      },
    ]
  }

];

/* 匹配任意的路由 必须最后注册 */
export const anyRoute = {
  path: '/:pathMatch(.*)',
  name: 'Any',
  redirect: '/404',
  meta: {
    hidden: true,
  },
};
