import request from '@/utils/request'; //导入请求接口的方法
//基础销售属性
interface BaseSaleAttrData {
  id?: number;
  name: string;
}

export type BaseSaleAttrListData = BaseSaleAttrData[];
//// spu图片 这段代码定义了 SpuImageData 接口:
// - 用于描述 SPU 图片信息
// - 包含 id、imgName、imgUrl、spuId 4个属性
// - id 和 spuId 是可选的
// - imgName 和 imgUrl 是必需的

export interface SpuImageData {
  id?: number; //属性 id 和 spuId 是一个可选元素 ?
  imgName: string;
  imgUrl: string;
  spuId?: number;
  name?: string;
  url?: string; //url 是一个可选的字符串
  response?: ResponseData; //response 是一个可选的 ResponseData 类型
  isDefault?: string; //isDefault 是一个可选的字符串
}
export interface ResponseData {
  code: number;
  data: string;
  message: string;
  ok: boolean;
}
//spu图片列表
export type SpuImageListData = SpuImageData[];
// spu中销售属性值 整个接口描述了一个商品的单个销售属性的数据结构
interface SpuSaleAttrValueData {
  baseSaleAttrId?: number;
  id?: number;
  isChecked?: string;
  saleAttrName?: string;
  saleAttrValueName: string;
  spuId?: number;
}
// spu中销售属性值列表
export type SpuSaleAttrValueListData = SpuSaleAttrValueData[];
// spu销售属性
// 整个接口描述了一个商品的单个销售属性的数据结构
export interface SpuSaleAttrData {
  baseSaleAttrId?: number | string;
  id?: number;
  saleAttrName: string;
  spuId?: number;
  spuSaleAttrValueList: SpuSaleAttrValueListData;
  isEdit?: boolean;
  saleAttrValueName?: string;
  saleAttrIdValueId?: string;
}
// spu中销售属性列表
export type SpuSaleAttrListData = SpuSaleAttrData[];
// spu
export type SpuData = {
  category3Id: string | number;
  description: string;
  id?: number;
  tmId?: number | string;
  spuName: string;
  spuImageList: SpuImageListData;
  spuSaleAttrList: SpuSaleAttrListData;
};
// spu列表
export type SpuListData = SpuData[];
// spu的分页列表
type SpuPageListData = {
  records: SpuListData;
  total: number;
  size: number;
  current: number;
  searchCount: boolean;
  pages: number;
};

// 四个接口
export default {
  // GET  /admin/product/baseSaleAttrList
  // getBaseSaleAttrList 获取基础销售属性列表  之后咱们完成spu增加销售属性时 需要从基础销售属性中进行选择
  getBaseSaleAttrList() {
    return request.get<any, BaseSaleAttrListData>(
      `/admin/product/baseSaleAttrList`
    );
  },

  // DELETE /admin/product/deleteSpu/{spuId}
  deleteSpu(spuId: number) {
    return request.delete<any, null>(`/admin/product/deleteSpu/${spuId}`);
  },

  // 1. 定义了 addOrUpdate 接口
  // 2. 通过 POST 请求不同的 API 来实现
  // 3. 根据 spu 中是否有 id 判断调用哪个 API
  // 4. 保存 SPU 时调用 / saveSpuInfo
  // 5. 更新 SPU 时调用 / updateSpuInfo
  // 6. 请求传入的参数为整个 spu 对象
  // 7. 返回值指定为 any、null类型

  addOrUpdate(spu: SpuData) {
    return request.post<any, null>(
      `/admin/product/${spu.id ? 'updateSpuInfo' : 'saveSpuInfo'}`,
      spu
    );
  },

  // GET /admin/product/{page}/{limit}
  // 获取spu的分页列表  留意要增加category3Id这个参数 是query参数!!!
  pageSpuList(page: number, limit: number, category3Id: number) {
    return request.get<any, SpuPageListData>(
      `/admin/product/${page}/${limit}?category3Id=${category3Id}`
    );
  },
};
