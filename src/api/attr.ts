import request from '@/utils/request'; // 引入封装好的 axios 请求

export interface AttrValueData {
  // 属性值
  id?: number; // 属性值id
  valueName: string; // 属性值名称
  attrId: number | undefined; // 属性id
  isEdit: boolean | undefined; // 是否可修改
}
export type AttrValueListData = AttrValueData[]; // 属性值列表
export interface AttrData {
  // isEdit: boolean; // 属性
  id?: number;
  attrName: string;
  categoryId: number;
  categoryLevel: number;
  attrValueList: AttrValueListData; // 属性值列表
  attrIdValueId?: string;
}
export type AttrListData = AttrData[]; // 属性列表

export default {
  // GET  /admin/product/attrInfoList/{category1Id}/{category2Id}/{category3Id}
  // attrInfoList
  // 根据一级分类id二级分类id三级分类id获取属性列表
  attrInfoList(category1Id: number, category2Id: number, category3Id: number) {
    // 参数类型
    return request.get<any, AttrListData>( // 返回值类型
      `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`
    );
  },

  // DELETE /admin/product/deleteAttr/{attrId}
  // deleteAttr
  // 删除属性
  deleteAttr(attrId: number) {
    // 参数类型
    return request.delete<any, null>(`/admin/product/deleteAttr/${attrId}`); // 返回值类型
  },

  // POST /admin/product/saveAttrInfo
  // saveAttrInfo
  // 添加属性和修改属性都使用这一个接口
  saveAttrInfo(attr: AttrData) {
    return request.post(`/admin/product/saveAttrInfo`, attr);
  },
};
