import request from "@/utils/request";

export interface TradeMarkData {
  id?: number;
  logoUrl: string;
  tmName: string;
}

interface TradeMarkPageListData {
  records: TradeMarkData[];
  total: number;
  size: number;
  current: number;
  searchCount: boolean;
  pages: number;
}

export default {
  // DELETE  /admin/product/baseTrademark/remove/{id}
  // 删除BaseTrademark
  deleteTm(id: number) {
    return request.delete<any, null>(
      `/admin/product/baseTrademark/remove/${id}`
    );
  },

  // 通过观察接口文档发现 无论是添加还是修改 都让咱们传递一个品牌对象 里面都有id是不对的
  // 因为id是后端生成的 添加时咱们前端没有id  修改时咱们是从后端获取过品牌列表的 里面的每个品牌都有id
  // 添加没有id  修改有id

  // POST /admin/product/baseTrademark/save
  // 新增BaseTrademark
  // PUT /admin/product/baseTrademark/update
  // 修改BaseTrademark
  addOrUpdate(trademark: TradeMarkData) {
    if (trademark.id) {
      //修改
      return request.put<any, null>(
        `/admin/product/baseTrademark/update`,
        trademark
      );
    } else {
      //添加
      return request.post<any, null>(
        `/admin/product/baseTrademark/save`,
        trademark
      );
    }
  },

  // GET /admin/product/baseTrademark/{page}/{limit}
  // 分页列表
  getPageList(page: number, limit: number) {
    return request.get<any, TradeMarkPageListData>(`/admin/product/baseTrademark/${page}/${limit}`);
  },

  getAllTrademarkList() {
    return request.get<any, TradeMarkData[]>(`/admin/product/baseTrademark/getTrademarkList`)
  }
};
