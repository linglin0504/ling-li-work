import request from "@/utils/request";

// 通过咱们测试接口 发现这三个接口返回的数据差不多
// 如果请求二级分类列表 里面的每一个对象都有category1Id   如果请求三级分类列表 里面的每一个对象都有category2Id
// 咱们统一定义他们的数据类型

interface CategoryData {
  id: number;
  name: string;
  category1Id?: number;
  category2Id?: number;
}

export type CategoryListData = CategoryData[];

export default {
  // GET  /admin/product/getCategory1
  // getCategory1
  getCategory1() {
    return request.get<any, CategoryListData>(`/admin/product/getCategory1`);
  },
  // GET /admin/product/getCategory2/{category1Id}
  // getCategory2
  getCategory2(category1Id: number) {
    return request.get<any, CategoryListData>(`/admin/product/getCategory2/${category1Id}`);
  },
  // GET /admin/product/getCategory3/{category2Id}
  // getCategory3
  getCategory3(category2Id: number) {
    return request.get<any, CategoryListData>(`/admin/product/getCategory3/${category2Id}`);
  },
};
