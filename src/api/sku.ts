import request from '@/utils/request'; //导入请求接口的方法
import type { SpuImageListData, SpuSaleAttrListData } from './spu';

interface SkuSaleAttrValueData {
  id?: number;
  skuId?: number;
  spuId?: number;
  saleAttrValueId?: number | string; //这里的类型是 number 或者 string
  saleAttrId?: number | string;
  saleAttrName?: string; //这里的类型是 string
  saleAttrValueName: string;
}
// sku销售属性值列表类型
export type SkuSaleAttrValueListData = SkuSaleAttrValueData[]; //这里的类型是 SkuSaleAttrValueData 数组
// sku平台属性值类型
interface SkuAttrValue {
  id?: number;
  attrId?: number | string;
  valueId?: number | string;
  skuId?: number;
  attrName?: string;
  valueName?: string;
}

// sku平台属性值列表类型
export type SkuAttrValueListData = SkuAttrValue[];

// sku图片类型
interface SkuImageData {
  id?: number;
  skuId?: number;
  imgName: string;
  imgUrl: string;
  spuImgId?: number;
  isDefault?: string;
}

// sku图片列表类型
export type SkuImageListData = SkuImageData[]; // sku图片列表类型

// sku类型
export interface SkuData {
  id?: number;
  spuId?: number;
  price: number | string; //number | string 表示这个属性可以是 number 类型,也可以是 string 类型。
  skuName: string;
  skuDesc: string;
  weight: string;
  tmId?: number;
  category3Id: number | string;
  skuDefaultImg: string;
  isSale?: number; //是否上架
  createTime?: string; //创建时间
  skuImageList: SkuImageListData;
  skuAttrValueList: SkuAttrValueListData;
  skuSaleAttrValueList: SkuSaleAttrValueListData;
}
// sku列表类型
export type SkuListData = SkuData[];

interface SkuPageListData {
  records: SkuListData;
  total: number;
  size: number;
  current: number;
  searchCount: boolean;
  pages: number;
}

export default {
  cancelSale(skuId: number) {
    return request.get<any, null>(`/admin/product/cancelSale/${skuId}`);
  },

  onSale(skuId: number) {
    return request.get<any, null>(`/admin/product/onSale/${skuId}`);
  },

  deleteSku(skuId: number) {
    return request.delete<any, null>(`/admin/product/deleteSku/${skuId}`);
  },

  findBySpuId(spuId: number) {
    return request.get<any, SkuListData>(`/admin/product/findBySpuId/${spuId}`);
  },

  getById(skuId: number) {
    return request.get<any, SkuData>(`/admin/product/getSkuById/${skuId}`);
  },

  pageSkuList(page: number, limit: number) {
    return request.get<any, SkuPageListData>(
      `/admin/product/list/${page}/${limit}`
    );
  },
  saveSkuInfo(sku: SkuData) {
    return request.post<any, null>(`/admin/product/saveSkuInfo`, sku);
  },
  getSpuImageList(spuId: number) {
    return request.get<any, SkuImageListData>(
      `/admin/product/spuImageList/${spuId}`
    );
  },

  getSpuSaleAttrList(spuId: number) {
    return request.get<any, SpuSaleAttrListData>(
      `/admin/product/spuSaleAttrList/${spuId}`
    );
  },
};
