import { defineStore } from 'pinia';
import { getToken, removeToken, setToken } from '../utils/token-utils';
import { ElMessage } from 'element-plus'; // 引入element-plus的message组件

import { staticRoutes, allAsyncRoutes, anyRoute } from '@/router/routes'; // 引入静态路由
import userApi from '@/api/user';
// import userApi,{type LoginParamsData} from "@/api/user";
import type { LoginParamsData, UserInfoData } from '@/api/user'; // 引入接口
import type { RouteRecordRaw } from 'vue-router'; // 引入路由接口
import router from '@/router';
import Item from '@/layout/components/Sidebar/Item';
//从 lodash 库中导入 cloneDeep 方法,按需导入的方式,可以避免导入整个 lodash 库,
//有利于缩小打包体积,只包含实际需要的代码。这在 Vue 或者 React 项目中是一种常见的优化手段
import cloneDeep from 'lodash/cloneDeep'; // 引入深拷贝

interface UserInfoStoreStataeData {
  token: string;
  userInfo: UserInfoData;
  menuRoutes: RouteRecordRaw[];
}
// 封装初始化userInfo这个数据的函数
function initUserInfo() {
  // 这个函数的作用是初始化userInfo这个数据
  return {
    routes: [], // 用户的路由
    buttons: [], // 用户的按钮
    roles: [], // 用户的角色
    name: '',
    avatar: '', // 用户头像
  };
}
// 封装一个函数 用来过滤路由
function filterUserRoutes(
  allAsyncRoutes: RouteRecordRaw[],
  userRoutesName: string[]
) {
  // 这个函数的作用是过滤路由
  let result = allAsyncRoutes.filter((Item) => {
    // 过滤路由
    if (userRoutesName.indexOf(Item.name as string) !== -1) {
      //
      if (Item.children && Item.children.length) {
        Item.children = filterUserRoutes(Item.children, userRoutesName);
      }
      return true;
    }
  });
  return result;
}
//把用户过滤出来的异步路由增加到路由器中
function addUerAsyncRouter(userAsyncRouter: RouteRecordRaw[]) {
  // 这个函数的作用是把用户过滤出来的异步路由增加到路由器中
  userAsyncRouter.forEach((Item) => {
    // 遍历用户过滤出来的异步路由
    router.addRoute(Item); // 把用户过滤出来的异步路由增加到路由器中
  });
}

//用户退出登录需要清除路由器中的路由，防止下一用户进来还能看到之前用户的路由
function clearRoutes() {
  // 获取所有的路由 返回值是所有路由所组成的数组
  let routes = router.getRoutes();
  // 清除所有路由
  routes.forEach((item) => {
    // 使用路由器实例身上的removeRoute方法 来去删除某个路由
    // 参数：要删除路由对象名name属性
    router.removeRoute(item.name as string);
  });
  // 增加静态路由
  addUerAsyncRouter(staticRoutes);
}
export const useUserInfoStore = defineStore('userInfo', {
  state: (): UserInfoStoreStataeData => ({
    token: localStorage.getItem('token') || '', //token
    userInfo: initUserInfo(), //用户信息
    menuRoutes: staticRoutes, // 应该放置路由对象数组 这个数据在控制菜单生成  菜单是根据路由生成!!!
    //路由对象中meta数据如果hidden:true  那么不会根据这个路由对象生成菜单
  }),

  // 1.修改userinfoStore中的数据及方法
  // 2.获取用户信息及退出登录需要token  请求拦截器中增加token
  // 3.点击登录触发actions方法login传递的参数不对 需要传递一个对象
  // 4.一直再获取用户信息发送请求 把permiss.ts文件中的数据修改
  // 5.home组件显示的内容不正确 navbar组件显示的内容不正确
  // 6.点击退出登录不能触发reset方法 而应该触发logout方法
  actions: {
    // 登录
    async login(loginParams: LoginParamsData) {
      try {
        const res = await userApi.login(loginParams);
        this.token = res.token;
        localStorage.setItem('token', res.token);
        return 'ok';
      } catch (error) {
        return Promise.reject('fail');
      }
    },
    // 获取用户信息
    async getInfo() {
      try {
        const res = await userApi.info();
        this.userInfo = res;
        let useAsyncRoutes = filterUserRoutes(
          cloneDeep(allAsyncRoutes),
          res.routes
        );
        // 2.把用户过滤出来的异步路由增加到路由器中 可以进行路由跳转
        addUerAsyncRouter(useAsyncRoutes.concat(anyRoute));
        // 3.根据现在用户的最新路由 生成最新菜单
        this.menuRoutes = staticRoutes.concat(useAsyncRoutes, anyRoute);

        return 'ok';
      } catch (error) {
        console.log(error);
        return Promise.reject('fail');
      }
    },
    // 重置用户相关 退出登录和token过期
    reset() {
      // 清除token(pinia以及localStorage) 清除yoghurt信息
      this.token = '';
      localStorage.removeItem('token');
      // console.log(this.userInfo,'this.userInfo')
      this.userInfo = Object.assign(this.userInfo, initUserInfo());
      // 之后何时使用Object.assign  如果对代理对象直接重新赋值时
    },
    // 退出登录
    async logout() {
      try {
        const res = await userApi.logout();
        this.reset();
        return 'ok';
      } catch (error) {
        return Promise.reject('fail');
      }
    },
  },
});
