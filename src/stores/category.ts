import { defineStore } from 'pinia' // 引入pinia
import categoryApi, { type CategoryListData } from '@/api/category'// 引入api
import { ElMessage } from 'element-plus'// 引入element-plus的message组件

interface CategoryStoreStataData {
    category1Id: number | string;
    category2Id: number | string;
    category3Id: number | string;
    category1List: CategoryListData;
    category2List: CategoryListData;
    category3List: CategoryListData;
}


export const useCategoryStore = defineStore('categorys', {// 定义store
    state: () => ({ // 定义state
        category1Id: '',// 分类id
        category1List: [],// 分类列表
        category2Id: '',// 分类id
        category2List: [],// 分类列表
        category3Id: '',// 分类id
        category3List: [],// 分类列表
    }),
    actions: { // 定义actions
        async getCategory1List() { // 获取分类列表
            try {     //  try catch 捕获异常
                const res = await categoryApi.getCategory1() // 调用api
                this.category1List = res;
            } catch (error) {
                ElMessage.error("获取一级分类列表失败") // 弹出错误信息
            }
        },
        async getCategory2List(category1Id: number) { // 获取分类列表
            this.category2Id = ''; // 清空二级分类id
            this.category3Id = '';// 清空三级分类id
            this.category3List = [];// 清空三级分类列表

            try {
                const res = await categoryApi.getCategory2(category1Id) // 调用api
                this.category2List = res
            } catch (error) {
                ElMessage.error("获取二级分类列表失败") // 弹出错误信息
            }
        },
        async getCategory3List(category2Id: number) {
            this.category3Id = ''; // 清空三级分类id
            // 获取分类列表
            try {
                const res = await categoryApi.getCategory3(category2Id) // 调用api
                this.category3List = res
            } catch (error) {
                ElMessage.error("获取三级分类列表失败") // 弹出错误信息
            }
        },
    },
    getters: { // 定义getters
    },
})