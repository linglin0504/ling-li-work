import axios, { type AxiosRequestHeaders, type AxiosResponse } from "axios";
import { ElMessage, ElMessageBox } from "element-plus";
import pinia from "@/stores/index";
import { useUserInfoStore } from "../stores/userInfo";

/* 定义response对象的data接口 */
// 这个T在约束response.data.data的类型 但是咱们此刻在拦截器中 没有办法知道这次请求是哪一个 数据就不固定 此刻咱们没有办法去定义response.data.data的类型
interface ResponseData<T> {
  code: number;
  data: T; //response.data.data 说白了就是咱们请求真正要用的那个数据
  message: string;
}

// 配置新建一个 axios 实例
const service = axios.create({
  baseURL: import.meta.env.VITE_API_URL, // '/app-dev' || '/app-prod'
  timeout: 50000,
});

// 添加请求拦截器
service.interceptors.request.use((config) => {
  // 如果咱们再组件中生成仓库对象 直接调仓库函数就可以
  // const userInfoStore = useUserInfoStore()
  // 如果咱们不再组件中生成仓库对象 调仓库函数时需要传递pinia
  const userInfoStore = useUserInfoStore(pinia)
  const token = userInfoStore.token
  if(token){
    (config.headers as AxiosRequestHeaders ).token = token
  }
  return config;
});

// 添加响应拦截器
service.interceptors.response.use(
  /* 约束一下response */
	// AxiosResponse是一个接口  接口泛型
	// 第一个传递的泛型在约束response.data的类型
  async (response:AxiosResponse<ResponseData<any>, any>) => {
    // 对响应数据做点什么
    const res = response.data;
    // res = {
    // 	code,
    // 	data,
    // 	message
    // }
    if (res.code !== 20000 && res.code !== 200) {
      /* 成功数据的code值为20000/200 */
      // 统一的错误提示
      ElMessage({
        message:
          (typeof res.data == "string" && res.data) || res.message || "Error",
        type: "error",
        duration: 5 * 1000,
      });

      // `token` 过期或者账号已在别处登录
      if (response.status === 401) {
        const storeUserInfo = useUserInfoStore(pinia);
        await storeUserInfo.reset();
        window.location.href = "/"; // 去登录页
        ElMessageBox.alert("你已被登出，请重新登录", "提示", {})
          .then(() => {})
          .catch(() => {});
      }
      return Promise.reject(service.interceptors.response);
    } else {
      return res.data; // 之后咱们发送请求获取到的结果其实是response.data.data  说白了就是后端真正给到咱们前端要去使用的数据  之后咱们也不可以res.code === 200的这个判断了
    }
  },
  (error) => {
    // 对响应错误做点什么
    if (error.message.indexOf("timeout") != -1) {
      ElMessage.error("网络超时");
    } else if (error.message == "Network Error") {
      ElMessage.error("网络连接错误");
    } else {
      if (error.response.data) ElMessage.error(error.response.statusText);
      else ElMessage.error("接口路径找不到");
    }
    return Promise.reject(error);
  }
);

export default service;
