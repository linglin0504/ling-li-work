const Mock = require("mockjs");
const levelData = require("@province-city-china/level");
const {
  province: Provinces,
  city: Cities,
  area: Areas,
} = require("province-city-china/data");
let data = {};

// 定义一个函数，用于从数组中随机选择一个元素
function randomPick(array) {
  return array[Math.floor(Math.random() * array.length)];
}
// 定义一个函数，用于从省市区数据中随机选择一个省市区对象
function randomArea() {
  // 随机选择一个省份对象
  let province = randomPick(levelData);
  let city, area;
  if (province && province.children && province.children.length > 0) {
    // 随机选择一个城市对象
    city = randomPick(province.children);
  } else {
    city = { name: "" };
  }
  if (city && city.children && city.children.length > 0) {
    // 随机选择一个区县对象
    area = randomPick(city.children);
  } else {
    area = { name: "" };
  }
  // 返回省市区对象
  return { province, city, area };
}

module.exports = () => {
  const categories = ["军事", "娱乐", "财经", "科技", "体育"];
  const images = [
    "http://39.98.123.211/group1/M00/03/D9/rBHu8mHmKC6AQ-j2AAAb72A3EO0942.jpg",
    "http://39.98.123.211/group1/M00/03/D9/rBHu8mHmKHOADErHAAAQBezsFBo612.jpg",
    "http://39.98.123.211/group1/M00/03/D9/rBHu8mHmKF2AWpcKAADv98DWYRo516.jpg",
    "http://39.98.123.211/group1/M00/02/DA/rBHu8mGxObeAfL10AAAsPY94Hn8745.png",
    "http://39.98.123.211/group1/M00/02/DA/rBHu8mGxOciADR75AAE6kN74a-E289.png",
    "http://39.98.123.211/group1/M00/02/DA/rBHu8mGxOdKAamtYAAC9HgX-V4c228.png",
    "http://39.98.123.211/group1/M00/02/DA/rBHu8mGxOduAL3-vAAAyNxaecgE342.jpg",
    "http://39.98.123.211/group1/M00/02/DC/rBHu8mGyMWmAMfrHAAAVOe83Fy0651.jpg",
    "http://39.98.123.211/group1/M00/02/DC/rBHu8mGyMX-AUh4sAAAH6bIsT_o073.jpg",
  ];
  const today = new Date(); // 获取当前时间的 Date 对象
  let userInfoList = [];
  data = Mock.mock({
    "newsList|100000": [
      {
        "id|+1": 1, // 新闻ID
        title: "@ctitle(20, 40)", // 新闻标题（长度在10到50之间）
        author: "@cname", // 作者名字
        "image|1": images, // 图片链接
        content: "@csentence(500, 1000)", //文章内容（长度在500到1000之间的句子）
        createTime: "@datetime", // 创建时间
        "category|1": categories, // 随机从categories数组中选择一个值
      },
    ],
    "orderInfoList|100000": [
      {
        "id|+1": 1,
        orderNo: '@string("number", 10)',
        createTime: "@datetime",
        receiverName: "@cname",
        receiverPhone: /^1[3-9]\d{9}$/,
        receiverAddress: "@province@city@county@zip",
        "orderItems|1-5": [
          {
            skuId: "@integer(1,10000)",
            "name|1": [
              "口罩",
              "健身时尚用品",
              "瑜伽垫",
              "可穿戴设备",
              "休闲服",
              "姿势矫正器",
              "塑身衣",
              "水壶",
              "手机壳",
              "手机三脚架",
              "车载电话架",
              "Apple AirPods Pro",
              "OPPO AR概念眼镜",
              "三合一智能助眠设备",
              "3D打印机",
              "索尼3D显示器",
              "Insta360 ONE X2袖珍相机",
              "户外安全摄像机",
            ],
            price: "@float(50, 100, 2, 2)",
            skuNum: "@integer(1, 5)",
            skuImage: "@image",
          },
        ],
        originalPrice: "@float(500, 1000, 2, 2)",
        discountAmount: "@float(10, 200, 2, 2)",
        totalPrice: function () {
          return (this.originalPrice - this.discountAmount).toFixed(2);
        },
        "status|1": ["待付款", "待发货", "待收货", "已完成"],
      },
    ],
  });
  for (let i = 1; i <= 200; i++) {
    const id = Mock.Random.id();
    const name = Mock.Random.cname();
    const gender = Mock.Random.integer(0, 1) + "";
    const birth = Mock.Random.date("yyyy-MM-dd");
    const birthDate = new Date(birth); // 将字符串转化为日期格式
    const age = today.getFullYear() - birthDate.getFullYear();
    let { province, city, area } = randomArea();
    let address = Mock.mock(`${province.name}${city.name}${area.name}`);

    const user = {
      id,
      name,
      gender,
      age,
      birth,
      province: province.name,
      city: city.name,
      area: area.name,
      address,
      sort: i,
    };
    userInfoList.push(user);
  }

  data.userInfo = userInfoList;
  // 获取所有省市区
  data.Provinces = Provinces;
  // 直辖市缺少城市这一层，解构修改数据
  data.Cities = [
    ...Cities,
    {
      code: "110000",
      name: "北京市",
      province: "11",
      city: "01",
    },
    {
      code: "120000",
      name: "天津市",
      province: "12",
      city: "01",
    },
    {
      code: "310000",
      name: "上海市",
      province: "31",
      city: "01",
    },
    {
      code: "500000",
      name: "重庆市",
      province: "50",
      city: "01",
    },
  ];
  data.Areas = Areas;
  return data;
};
