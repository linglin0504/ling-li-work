在 Vue 组件里面,const 和 import 关键字都有重要的作用:

1. const 用来声明常量,保证数据不会被修改。
   js
   // 模板中可以安全使用 count
   const count = 1
2. import 用来导入模块,实现代码的复用。
   js
   import HelloWorld from '@/components/HelloWorld'

// 可以在组件中使用 HelloWorld 组件了
const 的作用:

- 声明组件的常量数据
- 保证数据不会被意外修改
  import 的作用:
- 导入其他模块、组件、函数、类型等
- 实现代码的复用和解耦
- TypeScript 类型检查
