接口是一个定义对象形状的方法。通过接口,我们可以定义一个类要实现什么样的规范。

前端和后端的接口主要区别在于:

- 后端接口:
  后端接口指的是 HTTP API 接口,用于不同系统或客户端之间交换信息,是一种网络服务。
- 前端接口:
  前端接口指的是 Javascript 中定义对象形状的方式,定义对象应该具有哪些属性和方法。

前端接口的主要作用是:

1. 定义规范,让对象具有一致的形状和行为。
2. 为不同类型的对象提供一个抽象,让使用方只关心接口,不关心内部实现。
3. 方便 Typescript 类型检查,自动补全。
   当使用 API 进行跨域通信时:
4. 浏览器直接调用后端提供的 HTTP 接口。
5. 服务端会响应一个 JSON 数据字符串。
6. 前端通过接口来解析并处理 JSON 数据。

区别点总结:

- 后端接口: 定义跨网络的 HTTP API 服务
- 前端接口: 定义对象的形状,实现规范化
- API 跨域通信: 通过 HTTP 请求,前端与后端交换 JSON 数据。

前端 API 通信的基本原理如下:

1. 浏览器发起 AJAX 请求,向后端 API 发送 HTTP 请求。
2. 服务端接收到请求后,处理请求并生成响应数据。
3. 服务端将响应数据以 JSON 格式返回给浏览器。
4. 浏览器收到响应后,获取响应数据。
5. 前端根据需要对 JSON 数据进行解析处理,渲染为页面。

1 浏览器使用 XMLHttpRequest 对象发起 AJAX 请求:
js
let xhr = new XMLHttpRequest();
xhr.open('GET', url, true);
xhr.send();
2 服务端接收请求,处理数据,生成 JSON 格式的响应数据:
json
{
"name": "John",
"age": 30
}
3 服务端响应时指定 Content-Type 为 application/json :
http
HTTP/1.1 200 OK
Content-Type: application/json
4 浏览器获取响应数据(res.responseText) :
js
xhr.onload = function() {
let res = xhr.responseText;
}
5 前端解析 JSON 数据,按需渲染:
js
let data = JSON.parse(res);
console.log(data.name); // John
总的来说,前端 API 通信的核心是:

1. 通过 AJAX 请求,将 HTTP 请求发送到后端 API
2. 后端生成 JSON 数据作为响应
3. 前端通过解析 JSON 数据,处理接口返回值
