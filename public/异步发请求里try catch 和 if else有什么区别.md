在异步发请求中,try catch 和 if else 的区别主要在于:
1. 错误捕获的方式不同
- try catch 通过捕获错误来处理请求失败的场景
- if else 通过判断状态码来处理请求成功或失败
2. 处理范围不同
- try catch 可以捕获请求过程中的除了状态码以外的各种异常错误,如网络错误、JSON解析错误等
- if else 只能处理状态码判断的场景
3. 代码结构不同
- try catch 把处理逻辑集中在一起,没有嵌套
- if else 需要在每个请求后单独处理成功和失败分支  
4. 异步代码处理不同
- try catch 更适合 Promise 异步代码
- if else 更多用于回调函数异步代码
所以,对于现代 Promise + async/await 的异步请求来说,使用 try catch 能更好地统一处理请求异常,代码结构也更优雅。
而过去基于回调的异步请求,if else 把控制流处理得更明确